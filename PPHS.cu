__global__ void mnozenje (float *rezultat, float *mat1, float *mat2, int smat1, int s\
mat2)
{
  const int idx =  threadIdx.y * blockDim.x + threadIdx.x;
  int sum = 0;

  int i = 0;
  if (smat1 == 3)
    {
     i = smat1;
    }
  else
    {
      i = 2;
    }

  for (int k = 0; k < i; k++)
        {
          sum += mat1[threadIdx.x * smat1 + k] * mat2[k * smat2 + threadIdx.y];
        }
  rezultat[idx]=sum;
}
