import numpy as np
from numpy import linalg
import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import time

mod = SourceModule(open("PPHS.cu").read())

print('Unos koordinata na x osi...')
a = int(input ('Za tocku a: '))
b = int(input ('Za tocku b: '))
c = int(input ('Za tocku c: '))
print('Unos koordinata na y osi...')
a1 = int(input('Za tocku a: '))
b1 = int(input('Za tocku b: '))
c1 = int(input('Za tocku c: '))


A = np.array([[a, a**2], [b,b**2], [c, c**2]], dtype=np.float32)
B = np.array([[a1],[b1],[c1]], dtype=np.float32)
print("Matrica A = ",A)
print("Matrica B = ",B)

T = np.transpose(A).copy()
print("Transponirana matrica A, T =",T)

S_A = np.int32(A.shape[1])
S_B = np.int32(B.shape[1])
S_T = np.int32(T.shape[1])

start_gpu1 = time.time()

U  = np.empty((2,2), dtype=np.float32)
mnozenje = mod.get_function("mnozenje")
mnozenje(drv.Out(U), drv.In(T), drv.In(A), S_T, S_A, block=(2,2,1), grid=(2,1))
print ("Umnozak transponirane matrice T i matrice A, U = ", U)

V = np.empty((2,1), dtype=np.float32)
mnozenje(drv.Out(V), drv.In(T), drv.In(B), S_T, S_B, block=(2,2,1), grid=(1,1))
print ("Umnozak transponirane matrice T i matrice B, V =", V)

end_gpu1 = time.time()

I = linalg.inv(U)
print("Inverz U = ", I)

S_I = np.int32(I.shape[1])
S_V = np.int32(V.shape[1])

start_gpu2 = time.time()

N = np.empty((2,1), dtype=np.float32)
mnozenje(drv.Out(N), drv.In(I), drv.In(V), S_I, S_V, block=(2,1,1), grid=(1,1))
print ("Konacno rjesenje: ", N)

end_gpu2 = time.time()

print("Vrijeme na GPU = ", ((end_gpu1-start_gpu1)+(end_gpu2-start_gpu2)))

start_cpu = time.time()

U_cpu = np.dot(T,A)
V_cpu = np.dot(T,B)
N_cpu = np.dot(I, V_cpu)

end_cpu = time.time()

print("Vrijeme na CPU = ", end_cpu-start_cpu)

